local cmd = vim.cmd
local fn = vim.fn
local g = vim.g  -- global variables
local o = vim.o  -- global options
local b = vim.b  -- buffer options
local w = vim.wo -- window options

cmd("set encoding=utf-8")

--{{{
local plugin_names = {
    'ojroques/vim-oscyank',
    'vim-airline/vim-airline',
    'vim-airline/vim-airline-themes',
    'junegunn/vim-easy-align',

    'rkuslak/vim-reasonable',
    'scrooloose/nerdtree',
    'nathanaelkane/vim-indent-guides',
    -- 'jlanzarotta/bufexplorer',

    'tpope/vim-fugitive',
    'airblade/vim-gitgutter',

    'tpope/vim-commentary',
    'Raimondi/delimitMate',
    'michaeljsmith/vim-indent-object',

    'ekalinin/Dockerfile.vim',
    'cespare/vim-toml',

    'neovim/nvim-lspconfig',
    'hrsh7th/nvim-compe',
    'folke/trouble.nvim',
    -- 'mfussenegger/nvim-dap',
    -- 'rcarriga/nvim-dap-ui',
    'glepnir/lspsaga.nvim',

    'sainnhe/sonokai',

    'kyazdani42/nvim-web-devicons',
    'akinsho/nvim-bufferline.lua',

    'nvim-lua/popup.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope.nvim',
}
cmd "call plug#begin()"
    for _, plugin_name in pairs(plugin_names) do
        local plug_cmd = "Plug '" .. plugin_name .. "'"
        cmd(plug_cmd)
    end
cmd "call plug#end()"
--}}}

-- Convert this to Lua if we can find a way to pull the strings returned from
-- `ls`
cmd( [[
function! GetBufferList()
    redir =>buflist
    silent! ls
    redir END
    return buflist
endfunction

function! ToggleLoc()
    for bufnum in map(filter(split(GetBufferList(), '\n'), 'v:val =~ "Location List"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
        silent! lclose
        return
    endfor
    silent! lopen
endfunction
]])
-- function ToggleLoc()
--     local buffer_list = cmd("silent! ls")
--     if string.match(buffer_list, "Location List") then
--         cmd("silent! lclose")
--         return
--     end
--     cmd "silent! lopen"
-- end

cmd "set spell spelllang=en_us"

local status, bufferline = pcall(require, 'bufferline')
if status then bufferline.setup{} end

local status, compe = pcall(require, "compe")
if (status) then
    compe.setup {
      enabled = true;
      autocomplete = true;
      debug = false;
      min_length = 1;
      preselect = 'enable';
      throttle_time = 80;
      source_timeout = 200;
      resolve_timeout = 800;
      incomplete_delay = 400;
      max_abbr_width = 100;
      max_kind_width = 100;
      max_menu_width = 100;
      documentation = {
        border = { '', '' ,'', ' ', '', '', '', ' ' }, -- the border option is the same as `|help nvim_open_win|`
        winhighlight = "NormalFloat:CompeDocumentation,FloatBorder:CompeDocumentationBorder",
        max_width = 120,
        min_width = 60,
        max_height = math.floor(vim.o.lines * 0.3),
        min_height = 1,
      };

      source = {
        spell = true;
        path = true;
        buffer = true;
        calc = true;
        nvim_lsp = true;
        nvim_lua = true;
        vsnip = true;
        ultisnips = true;
        luasnip = true;
      };
    }
end

local function map(mapKey, mapCommand, opts, bind_scope)
    options = {noremap = true, silent = true}
    if opts then
        for key, value in pairs(opts) do
            options[key] = value
        end
    end

    if not bind_scope then
        bind_scope = ""
    end

    vim.api.nvim_set_keymap(bind_scope, mapKey, mapCommand, options)
end

local status, lspconfig = pcall(require, "lspconfig")
if status then
    lspconfig.pyright.setup{}

    lspconfig.rust_analyzer.setup{}

    lspconfig.tsserver.setup{}
    lspconfig.jsonls.setup{}
    lspconfig.cssls.setup{}
    lspconfig.html.setup{}
    -- lspconfig..setup{}
end
local status, trouble = pcall(require, "trouble")
if status then
    trouble.setup{}
end

-- vim.o.completeopt = "menuone,noinsert,noselect"
o.completeopt = "menuone,noselect"

g.indent_guides_enable_on_vim_startup = 1
g.indent_guides_guide_size = 1

-- Until we port this from the vimscript of all things, this is not useful
-- map("<F3>", "<esc>:call FormatBuffer()<cr>")
map("<F3>", "<esc>:lua vim.lsp.buf.formatting()")
map("<F8>", "<esc>:NERDTreeToggle<cr>")

map("<leader>bb", "<esc>:split term://multibuilder -v -t build -r 1 -s %<cr>G")
map("<leader>bB", "<esc>:split term://multibuilder -v -t build -s %<cr>G")
map("<leader>bT", "<esc>:split term://multibuilder -v -t test -s %<cr>G")
map("<leader>bt", "<esc>:Telescope buffers<cr>")
map("<leader>tm", "<esc>:vsplit term://zsh<cr>a")
map("<leader>gg", "<esc>:vsplit term://gitui<cr>a")
map("<leader>cw", "<esc>:call CleanWhitespace()<cr>")
-- map("<leader>ll", "<esc>:call ToggleLoc()<cr>")
map("<leader>ld", "<esc>:lua vim.lsp.buf.definition()<cr>")
map("<leader>ls", "<esc>:lua vim.lsp.buf.signature_help()<cr>")
map("<leader>ll", "<esc>:TroubleToggle lsp_document_diagnostics<cr>")
map("<leader>lL", "<esc>:TroubleToggle lsp_workspace_diagnostics<cr>")
map("<leader>lr", "<esc>:lua vim.lsp.buf.references()<cr>")
map("<leader>li", "<esc>:lua vim.lsp.buf.implementation()<cr>")
map("<leader>lh", "<esc>:Lspsaga hover_doc<cr>", nil, "n")
-- map("<leader>lh", "<esc>:lua vim.lsp.buf.hover()<cr>", nil, "n")
map("<leader>ln", "<esc>:lua vim.lsp.buf.rename()<cr>")
map("<leader>yy", ":OSCYank<cr>j", nil, "v")
map("<leader>ca", "<esc>:call vim.lsp.buf.code_actions()<cr>")

local tabwidth = 4
g.shiftwidth = tabwidth
g.tabstop = tabwidth
g.expandtab = true

w.number = true

o.clipboard = "unnamedplus"
o.termguicolors = true

o.foldmethod = "indent"
o.foldnestmax = 10
o.foldlevel = 99

-- Themeing
g.airline_theme = "cobalt2"
g.sonokai_enable_italics = 1
g.sonokai_diagnostic_text_highlight = 1
g.sonokai_diagnostic_line_highlight = 1
cmd([[
    highlight clear
    silent! colorscheme sonokai
    highlight ColorColumn  ctermbg=231 guibg=#171717
    highlight CursorColumn ctermbg=251 guibg=#222222
    highlight CursorLine   ctermbg=8 guibg=#222222
]])

