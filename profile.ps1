<#
    Powershell Profile
    Ron Kuslak 2018-02-16
#>

function Prompt {
    "[${env:USERNAME}] " + ((Get-Date).ToString('yy-MM-dd hh:mmtt'))  + "`n" + `
        (Get-Location) + "> "
}

function Invoke-FzfCode {
    $file = $(fzf)
    if (-not [String]::IsNullOrWhiteSpace($file)) {
        code $file
    }
}

Set-PSReadLineOption -EditMode Emacs
