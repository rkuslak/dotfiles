if filereadable(expand("~/venv/bin/python3"))
    let g:python3_host_prog = expand( "~/venv/bin/python3")
endif

set completeopt=noinsert,menuone,noselect

set formatoptions-=t

"
" Vim Plug Setup: {{{

    call plug#begin()
    " General QOL:
    " Plug 'bling/vim-bufferline'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'junegunn/vim-easy-align'
    " Plug 'junegunn/fzf'
    " Plug 'junegunn/fzf.vim'
    " Plug 'editorconfig/editorconfig-vim'
    " Plug 'dhruvasagar/vim-table-mode'
    Plug 'rkuslak/vim-reasonable'
    Plug 'scrooloose/nerdtree'
    Plug 'nathanaelkane/vim-indent-guides'
    " Plug 'lukas-reineke/indent-blankline.nvim'
    " Plug 'Yggdroot/indentLine'
    " Plug 'junegunn/rainbow_parentheses.vim'
    Plug 'jlanzarotta/bufexplorer'

    " Git status plugin for NerdTree:
    " Plug 'Xuyuanp/nerdtree-git-plugin'
    Plug 'tpope/vim-fugitive'
    Plug 'airblade/vim-gitgutter'

    " Themes:
    " Plug 'Reewr/vim-monokai-phoenix'
    " Plug 'sickill/vim-monokai'
    " Plug 'tomasr/molokai'
    " Plug 'skielbasa/vim-material-monokai'
    Plug 'patstockwell/vim-monokai-tasty'

    " Syntax
    Plug 'ekalinin/Dockerfile.vim'
    Plug 'cespare/vim-toml'
    Plug 'vim-python/python-syntax'

    " Plug 'autozimu/LanguageClient-neovim', {
    "     \ 'branch': 'next',
    "     \ 'do': 'make release',
    " \ }
    Plug 'ncm2/ncm2'
    Plug 'roxma/nvim-yarp'
    Plug 'ncm2/ncm2-bufword'
    Plug 'ncm2/ncm2-path'
    " Plug 'ncm2/ncm2-vim-lsp'
    " Plug 'hrsh7th/nvim-compe'


    " General Coding:
    " Plug 'w0rp/ale'
    Plug 'tpope/vim-commentary'
    Plug 'Raimondi/delimitMate'
    Plug 'michaeljsmith/vim-indent-object'

    " Language Specific:
    Plug 'rust-lang/rust.vim',         { 'for': 'rust'}
    Plug 'sebdah/vim-delve',           { 'for': 'go'}

    " Plug 'mattn/emmet-vim/',           { 'for': ['html', 'css', 'javascript', 'typescript', 'jsx']}
    " Plug 'leafgarland/typescript-vim', { 'for': ['html', 'javascript', 'typescript', 'jsx']}
    Plug 'maxmellon/vim-jsx-pretty'
    Plug 'HerringtonDarkholme/yats.vim'
    " Plug 'pangloss/vim-javascript'
    " Plug 'mxw/vim-jsx'
    " Plug 'othree/html5.vim'
    " Plug 'lilydjwg/colorizer',         { 'for': ['css', 'html'] }
    " Plug 'posva/vim-vue'
    " Plug 'Quramy/tsuquyomi'
    " Plug 'mhartington/nvim-typescript', { 'for': ['html', 'javascript', 'typescript']}

    " Plug 'fatih/vim-go',               { 'for': ['go']}

    call plug#end()
" }}}

" {{{ Autocompletion
    " " LanguageClient:
    " let g:python_highlight_all = 1
    " let g:LanguageClient_serverCommands = {}

    " let g:LanguageClient_serverCommands['python'] = ['python3', '-m', 'pyls']

    " if executable('ra_lsp_server')
    "     let g:LanguageClient_serverCommands['rust'] = ['ra_lsp_server']
    " elseif executable('rls')
    "     let g:LanguageClient_serverCommands['rust'] = ['rls']
    " endif

    " if executable('gopls')
    "     let g:LanguageClient_serverCommands['go'] = ['gopls']
    "     autocmd BufWritePre *.go LspDocumentFormatSync
    " endif

    " if executable('clangd')
    "     let g:LanguageClient_serverCommands['c'] = ['clangd']
    "     let g:LanguageClient_serverCommands['cpp'] = ['clangd']
    "     let g:LanguageClient_serverCommands['objc'] = ['clangd']
    "     let g:LanguageClient_serverCommands['objcpp'] = ['clangd']
    " endif

    " if executable('typescript-language-server')
    "     let g:LanguageClient_serverCommands['javascript'] = ['typescript-language-server', '--stdio']
    "     let g:LanguageClient_serverCommands['javascriptreact'] = ['typescript-language-server', '--stdio']
    "     let g:LanguageClient_serverCommands['typescript'] = ['typescript-language-server', '--stdio']
    "     let g:LanguageClient_serverCommands['typescriptreact'] = ['typescript-language-server', '--stdio']
    " endif

    " let g:LanguageClient_serverCommands['json'] = ['vscode-json-languageserver', '--stdio']

    " NCM2:
    autocmd BufEnter * call ncm2#enable_for_buffer()
    " set completeopt=noinsert,menuone,noselect
" }}}

let g:bufferline_rotate = 1

" let g:ale_linters = {}
" let g:ale_fixers = {}

" let g:ale_linters['rust'] = ['rls', 'cargo', 'rustc']
" let g:ale_linters['python'] = ['mypy', 'pyls']
" let g:ale_linters['c'] = ['clangd']
" let g:ale_linters['cpp'] = g:ale_linters['c']
" let g:ale_linters['go'] = ['gopls', 'gofmt']

" let g:ale_fixers['rust'] = ['rustfmt']
" let g:ale_fixers['python'] = ['black', 'isort']
" let g:ale_fixers['c'] = ['clang-format', 'clangtidy']
" let g:ale_fixers['cpp'] = g:ale_fixers['c']
" let g:ale_fixers['go'] = ['goimports', 'gofmt']
" let g:ale_completion_enabled = 1

let g:python_highlight_all = 1

let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1

" Themeing: {{{
    " Airline theme
    let g:airline_theme="cobalt2"
    highlight clear
    set termguicolors
    let g:vim_monokai_tasty_italic = 1
    silent!   colorscheme  vim-monokai-tasty
    highlight ColorColumn  ctermbg=231 guibg=#171717
    highlight CursorColumn ctermbg=251 guibg=#222222
    highlight CursorLine   ctermbg=8 guibg=#222222

    set foldmethod=indent
    set foldnestmax=10
    set foldlevel=99
    set guifont=Fira\ Mono\ Medium:h9
" }}}

nnoremap <F3> <esc>:call FormatBuffer()<cr>
noremap <F8> :NERDTreeToggle<cr>

set shiftwidth=4 tabstop=4
autocmd filetype org setlocal shiftwidth=2
autocmd filetype cpp setlocal shiftwidth=2 tabstop=2
autocmd filetype yaml setlocal shiftwidth=2 tabstop=2
autocmd filetype vim nnoremap <buffer> <F5> :so %<cr>

autocmd filetype cs, ps1, psm1, cpp, c, rust, typescript, javascript, html, go, python RainbowParentheses

" Project: Move debugging into VIM instead of the console
autocmd filetype go     noremap <F9> <esc>:DlvToggleBreakpoint<cr>
autocmd filetype go     noremap <F5> <esc>:DlvDebug<cr>

" Defines special formatting programs for languages not covered by a LSP, or
" calls LSP formatter otherwise.
function! FormatBuffer()
    let l:save_pos = winsaveview()

    let l:buff_type = &filetype
    if l:buff_type == "sql"
        %!sqlformat -d "    " -m 80 -s 4 -j --keywordStandardization
    " " My stubbornness on space-indenting is only matched by my insistance on
    " " standardized formatting :(
    " elseif l:buff_type == "go"
    "     %!goimports
    "     " silent! %s/\t/    /g
    elseif l:buff_type == "c" || l:buff_type == "cpp" || l:buff_type == 'cxx' || l:buff_type == "h"
        %!clang-format -style=file
    else
        " LspDocumentFormat
        call LanguageClient#textDocument_formatting()
        " ALEFix
    end

    call winrestview(l:save_pos)
endfunction

function! CleanWhitespace()
    let save_pos = getpos(".")

    " Clean up 'extra' white space at the end of lines, and thin out lines that
    " are nothing but whitespace.
    silent! %s/\s*$//g
    silent! %s/\n\{3,}/\r\r/g
    :noh
    call setpos('.', save_pos)
endfunction

map <leader>bb <esc>:split term://multibuilder -v -t build -r 1 -s %<cr>G
map <leader>bB <esc>:split term://multibuilder -v -t build -s %<cr>G
map <leader>bT <esc>:split term://multibuilder -v -t test -s %<cr>G
map <leader>tm <esc>:vsplit term://zsh<cr>a
map <leader>gg <esc>:vsplit term://gitui<cr>a
map <leader>cw <esc>:call CleanWhitespace()<cr>
map <leader>ll <esc>:call ToggleLoc()<cr>
map <leader>lr <esc>:call LanguageClient#textDocument_references()<cr>
map <leader>lh <esc>:call LanguageClient#textDocument_hover()<cr>
map <leader>ca <esc>:call LanguageClient#textDocument_codeAction()<cr>
map <leader>rn <esc>:call LanguageClient#textDocument_rename()<cr>
" map <leader>lr <esc>:ALEFindReferences<cr>
" map <leader>lh <esc>:ALEHover<cr>


function! s:GetBufferList()
    redir =>buflist
    silent! ls
    redir END
    return buflist
endfunction

function! ToggleLoc()
    for bufnum in map(filter(split(s:GetBufferList(), '\n'), 'v:val =~ "Location List"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
        silent! lclose
        return
    endfor
    silent! lopen
endfunction

set textwidth=80

if has('win32')
    silent! so ~\\AppData\\Local\\nvim\\secrets.vim
else
    silent! so ~/.config/nvim/secrets.vim
    set clipboard=unnamedplus
endif

" XML pretty print:
function! DoPrettyXML()
  " save the filetype so we can restore it later
  let l:origft = &ft
  set ft=
  " delete the xml header if it exists. This will
  " permit us to surround the document with fake tags
  " without creating invalid xml.
  1s/<?xml .*?>//e
  " insert fake tags around the entire document.
  " This will permit us to pretty-format excerpts of
  " XML that may contain multiple top-level elements.
  0put ='<PrettyXML>'
  $put ='</PrettyXML>'
  silent %!xmllint --format -
  " xmllint will insert an <?xml?> header. it's easy enough to delete
  " if you don't want it.
  " delete the fake tags
  2d
  $d
  " restore the 'normal' indentation, which is one extra level
  " too deep due to the extra tags we wrapped around the document.
  silent %<
  " back to home
  1
  " restore the filetype
  exe "set ft=" . l:origft
endfunction
command! PrettyXML call DoPrettyXML()

" if filereadable(expand("~/local/bin/python3"))
"     let g:python3_host_prog = expand('~/local/bin/python3')
" endif

