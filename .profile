export PATH=${PATH}:~/local/bin

Platform=`uname`
alias ls="ls --color"

if [ "x${Platform}" = "xDarwin" ]; then
    # Add VSCode to path if we have it
    VSCODE_PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
    if [ -e "$VSCODE_PATH" ]; then
       export PATH=$PATH:${VSCODE_PATH}
    fi

    alias ls="ls -G "
fi

export EDITOR=nvim

##################
## Telemetry
##################

# Homebrew now has analytics. No, really.
export HOMEBREW_NO_ANALYTICS=1

# Opt out of Telemetry for .Net Core:
export DOTNET_CLI_TELEMETRY_OPTOUT=1

##################
## Add paths
##################

# Add our local clang/llvm/lldb/python
if [ -d "$HOME/.local/bin" ]; then
    export PATH="$HOME/.local/bin:$PATH"
    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HOME}/local/lib"
fi

if [ -d "$HOME/local/bin" ]; then
    export PATH="$HOME/local/bin:$PATH"
    export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HOME}/local/lib"
fi

# Add Go bin path
if [ -d "${GOPATH}/bin" ]; then
    export GOPATH=${HOME}/go
    export PATH="$PATH:${GOPATH}/bin"
fi

# Add rust to path
if [ -d "$HOME/.cargo" ]; then
    export PATH="$HOME/.cargo/bin:${PATH}"
fi
export PATH=${PATH}:~/homebrew/bin

