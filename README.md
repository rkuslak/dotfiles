# Dotfiles - A currated collection of configuration files and utility scripts

While most of the contents of this directory are of limited public interest, certain scripts under the "buildscripts"
directory have good general use. This is a repository of VM build automation and configuration files I use to rapid
stand up new VMs for development or remote access. Additional scripts, for example, to build and install a local copy
of python or LLDB exist in the buildscripts directory. These files are provided under a GPL v2 license unless otherwise
indicated.

* TODO

  * Complete Samba domain controller install scripts, and add build scripts for centos Samba 4
  * Complete build script for neovim to track HEAD?
