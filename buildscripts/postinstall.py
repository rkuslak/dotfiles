#!/usr/bin/env python3
"""
    A script to automate linking of various repo-resident dot files.
    (C)opyright 2017, 2018, 2019 Ron Kuslak, all rights reserved.

    Copyright 2018, 2019 Ron Kuslak

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
"""

import os
import pathlib
import platform
import shutil
import subprocess

import buildtools
from buildtools import CONFIGS_PATH


def main():
    if not os.name == "nt":
        # Linux or Mac; Ensure we've linked all dotfiles. Drop powershell
        # config if we have it
        dotfiles = [
            ".bashrc",
            ".gitconfig",
            ".profile",
            ".tmux.conf",
            ".zshrc",
        ]
        if platform.system() != "Darwin":
            dotfiles.append(".toprc")

        dotfiles = [CONFIGS_PATH / dotfile for dotfile in dotfiles]
        buildtools.link_dotfiles(dotfiles, os.getenv("HOME"))

        # If we have powershell, install config file
        if shutil.which("pwsh"):
            print("\nCreating PowerShell profile:")
            profile_path = CONFIGS_PATH / "profile.ps1"
            profile_arg = f"'ln -svf \"{profile_path}\" $PROFILE'"
            subprocess.call(["pwsh", "-C", profile_arg])
    else:
        home_path = pathlib.Path(os.getenv("USERPROFILE"))

        powershell_path = home_path / "Documents" / "WindowsPowerShell"
        powershell_path.mkdir(parents=True, exist_ok=True)

        powershell_profile_path = home_path / "Microsoft.PowerShell_profile.ps1"
        wslconfig_path = home_path / ".wslconfig"

        buildtools.link_file(CONFIGS_PATH / "profile.ps1", powershell_profile_path)
        buildtools.link_file(CONFIGS_PATH / ".wslconfig", wslconfig_path)

    return


if __name__ == "__main__":
    main()
