#!/usr/bin/env python3
"""
.
"""

import pathlib
import subprocess

DNF_CMD = [
    "dnf",
    "-y",
]

# VSCode:
REPO_FILE_TEXT = """
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
"""

YUM_REPOS_PATH = pathlib.Path("/etc/yum.repos.d/")

if __name__ == "__main__":
    repo_file_path = YUM_REPOS_PATH / "vscode.repo"
    with open(repo_file_path, "w") as repo_file:
        repo_file.write(REPO_FILE_TEXT)

    subprocess.check_call(
        DNF_CMD
        + [
            "update",
        ]
    )
    subprocess.check_call(DNF_CMD + ["install", "code"])
