#!/usr/bin/env python3
"""
    A simple script to automate build processes for a RPM based system.
    Copyright (C) 2018 Ron Kuslak

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation version 2.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along with
    this program; if not, write to the Free Software Foundation, Inc., 51
    Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

    TODO:
        Debian/Ubuntu reqs needed
        CentOS / Fedora reqs need to be validated (only work right now for
        certain  if we assume we're installing everything)
        Redirect completed compiles to /opt/${PKG}?
        Write some tracking file for what's installed? Getting dangerously
        close to a "real" package system
        Migrate Python 3, Clang/LLVM and LLDB into this? Bootstrap issues?
"""

import argparse
import functools
import os.path
import pathlib
import shutil
import subprocess
from enum import IntEnum
from typing import List, Optional

_INSTALLED_PACKAGES: List[str] = []
_PREFIX_PATH = pathlib.Path().home() / "local"
_BIN_PATH = _PREFIX_PATH / "bin"
_LIB_PATH = _PREFIX_PATH / "lib"
_REPO_PATH = _PREFIX_PATH / "repos"
PREFIX_PATH = os.path.join(str(pathlib.Path.home()), "local")
BIN_PATH = os.path.join(PREFIX_PATH, "bin")
LIB_PATH = os.path.join(PREFIX_PATH, "lib")


class GmakeBuilder:
    installer_commandline = ["dnf", "-y", "install"]

    def __init__(self, package_name, repo_url, required_packages, tag=None):
        self._required_packages = required_packages
        self._repo_url = repo_url
        self._package_name = package_name
        self._tag = tag

    @property
    def build_dir(self):
        return _REPO_PATH / self._package_name

    @property
    def install_dir(self):
        if self._tag:
            return _PREFIX_PATH / "packages" / f"{self._package_name}-{self._tag}"
        return _PREFIX_PATH / "packages" / f"{self._package_name}"

    def install_needed_packages(self):
        """
        Attempts to install dependancies for this package.
        """
        installed_packages = init_pkgs()
        needed_packages = [
            pkg for pkg in self._required_packages if not pkg in installed_packages
        ]

        if needed_packages:
            subprocess.check_call(self.installer_commandline + needed_packages)

    def pull_repo(self):
        """
        Attempts to pull or update the given repo. Raises CallProcessError
        if unable to successfully pull the repo or checkout the current tag.
        """
        if self.build_dir.exists():
            git_cmd = ["git", "pull"]

        else:
            self.build_dir.mkdir(exist_ok=True, parents=True)
            git_cmd = ["git", "clone", "--recursive", self._repo_url, "."]

        subprocess.check_call(git_cmd, cwd=build_dir)
        if self._tag:
            checkout_cmds = ["git", "checkout", "tags/" + tag]
            subprocess.check_call(checkout_cmds, cwd=build_dir)

    def configure(self, autogen=True):
        pass

    def build(self):
        """
        Builds and installs the package to a local package store
        """
        build_cmds = []

        # CMAKE flags should make sure we set them for projects that need them
        # and be silently ignored where not. Watch this be a incorrect
        # assumption.
        build_cmd.append(
            [
                "make",
                'CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=' + install_dir + '"',
                "-j4",
            ]
        )
        build_cmds.append(["make", "install"])

        for cmd_set in build_cmds:
            subprocess.check_call(cmd_set, cwd=self.build_dir)

    def link_package(self):
        link_dir(self.install_dir, _PREFIX_PATH)


class GmakeConfigBuilder(GmakeBuilder):
    def configure(self):
        configure_cmds = [
            self.build_dir / "configure",
            self.build_dir / "bootstrap",
        ]
        autogen_cmd = self.build_dir / "autogen.sh"

        for cmd in configure_cmds:
            if cmd.exists():
                build_cmd = [cmd, f"--prefix={self.install_dir}"]
                subprocess.check_call(build_cmd, cwd=self.build_dir)
                return

        if autogen_cmd.exists() and autogen:
            subprocess.check_call([autogen_cmd], cwd=self.build_dir)
            self.configure(autogen=False)


def init_pkgs() -> List[str]:
    """
    Returns a unversioned list of installed packages on the system currently.
    """

    pkgs_bytes = subprocess.check_output(["rpm", "-qa", "--qf", "%{NAME}\n"])
    pkgs_str = pkgs_bytes.decode("utf-8")
    pkgs = [pkg.strip() for pkg in pkgs_str.split("\n") if pkg]

    # Ensure we return a copy of the list, so outside changes do not propagate
    # down (if any)
    return pkgs


def package_needed(pkg: str, installed_pkgs: Optional[List[str]] = None) -> bool:
    """
    Test if given package is installed for this RedHat-based system.
    """
    installed_pkgs = installed_pkgs or init_pkgs()

    return bool(pkg in installed_pkgs)


def clone_git(repo_url: str, build_dir, tag: str = None) -> bool:
    """
    Clones the specified git repo URL locally. Returns True if clone
    was successful, False otherwise.
    """

    build_dir = pathlib.Path(build_dir)
    build_dir.mkdir(parents=True, exist_ok=True)

    subprocess.call(["git", "clone", "--recursive", repo_url, "."], cwd=build_dir)

    if tag:
        checkout_cmds = ["git", "checkout", "tags/" + tag]
        return subprocess.call(checkout_cmds, cwd=build_dir) == 0

    return True


def link_files(filelist: List[str], dest_dir: str) -> bool:
    """
    Create symbolic links for dotfiles
    """
    if os.name == "nt":
        return False

    for filename in filelist:
        _, basename = os.path.split(filename)
        if not basename:
            raise ValueError("No basename for path " + filename)

        dest_filename = os.path.join(dest_dir, basename)
        print("Linking " + filename + " to " + dest_filename + "...")

        try:
            os.symlink(filename, dest_filename)
            print("Linked file " + filename + " to " + dest_filename)
        except FileExistsError:
            os.unlink(dest_filename)
            os.symlink(filename, dest_filename)
            print("Linked file " + filename + " to " + dest_filename)

    return True


def build_gmake_config(
    build_dir: str, install_dir: str, bootstrap: bool = False, autogen: bool = False
) -> bool:
    """
    Builds a package using ./configure | make | make install
    """

    os.makedirs(install_dir, exist_ok=True)

    if autogen:
        if subprocess.call(["sh", "autogen.sh"], cwd=build_dir) != 0:
            return False

    configure = "./configure"
    if bootstrap:
        configure = "./bootstrap"

    config_cmds = [configure, "--prefix=" + install_dir]
    if (
        subprocess.call(config_cmds, cwd=build_dir) != 0
        or subprocess.call(["make", "-j4"], cwd=build_dir) != 0
        or subprocess.call(["make", "install"], cwd=build_dir) != 0
    ):
        return False

    # Build succeeded and installed; remove build directory and link binaries
    # in path-resident directory
    shutil.rmtree(build_dir)
    # bin_dir = os.path.join(install_dir, 'bin')
    # link_dir(bin_dir, BIN_PATH)
    link_dir(install_dir, PREFIX_PATH)

    return True


def build_gmake(build_dir: str, install_dir: str) -> bool:
    """ Builds a package using make | make install """
    global BIN_PATH

    os.makedirs(install_dir, exist_ok=True)

    build_opts = [
        "make",
        'CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=' + install_dir + '"',
    ]
    if (
        subprocess.call(build_opts, cwd=build_dir) != 0
        or subprocess.call(["make", "install"], cwd=build_dir) != 0
    ):
        return False

    # Build succeeded and installed; remove build directory and link binaries
    # in path-resident directory
    shutil.rmtree(build_dir)
    # bin_dir = os.path.join(install_dir, 'bin')
    # link_dir(bin_dir, BIN_PATH)
    link_dir(install_dir, PREFIX_PATH)

    return True


def link_dir(install_dir: str, dest_dir: str) -> None:
    """
    Links the contents of the passed install_dir path into the "prefix"
    directory dest_dir. Assumes the destination directory already exists.
    Links the 'bin' and 'lib' directories.
    """

    for dir_name in ["bin", "lib"]:
        bin_path = os.path.join(install_dir, dir_name)
        dest_path = os.path.join(dest_dir, dir_name)

        if not os.path.exists(bin_path):
            continue

        dir_list = os.listdir(bin_path)
        bin_files = [os.path.join(bin_path, f) for f in dir_list]
        link_files(bin_files, dest_path)


def installed_needed_packages(required_packages: List[str]):
    """Installs all needed packages from the list of passed package names
    on a RedHat - based system.
    """
    needed_packages = list(filter(package_needed, required_packages))

    dnf_cmd = None
    if shutil.which("dnf"):
        dnf_cmd = "dnf"
    elif shutil.which("yum"):
        dnf_cmd = "yum"
    else:
        raise EnvironmentError

    if needed_packages:
        cmd = ["sudo", dnf_cmd, "-y", "install"] + needed_packages
        print(str.join(" ", cmd))
        subprocess.run(cmd)


def build(pkgs: List[str]) -> None:
    """Main engine to build all desired local packages."""
    global PREFIX_PATH

    os.makedirs(BIN_PATH, exist_ok=True)

    # MAKE A LOT OF NOISE THIS NEEDS TO GO AWAY AND BE ABSTRACTED OUT OMG ##
    # Move the build info into classes
    # Automate running through them
    # Some means to determine if we've already installed?

    if "git" in pkgs:
        # Git
        required_packages = [
            "dh-autoreconf",
            "libcurl-devel",
            "expat-devel",
            "gettext-devel",
            "openssl-devel",
            "perl-devel",
            "zlib-devel",
            "asciidoc",
            "xmlto",
            "docbook2",
        ]
        build_dir = os.path.join(PREFIX_PATH, "git")
        install_dir = os.path.join(PREFIX_PATH, "packages/git-v2.18.0")
        clone_git("https://github.com/git/git", build_dir, "v2.18.0")

        make_cmd = ["make", "prefix=" + install_dir]
        subprocess.check_call(make_cmd, cwd=build_dir)
        subprocess.check_call(make_cmd + ["install"], cwd=build_dir)
        shutil.rmtree(build_dir)
        link_dir(install_dir, PREFIX_PATH)

    if "cmake" in pkgs:
        # CMake
        required_packages = [
            "gcc-c++",
            "make",
            "doxygen",
            "graphviz",
            "libxml2",
            "libedit-devel",
            "swig",
            "python-devel",
        ]
        installed_needed_packages(required_packages)
        installed_needed_packages(required_packages)
        build_dir = os.path.join(PREFIX_PATH, "cmake")
        install_dir = os.path.join(PREFIX_PATH, "packages/cmake-v3.12.2")
        if clone_git(
            "https://gitlab.kitware.com/cmake/cmake.git", build_dir, "v3.12.2"
        ):
            build_gmake_config(build_dir, install_dir, bootstrap=True)

    if "ninja" in pkgs:
        # Ninja
        build_dir = os.path.join(PREFIX_PATH, "ninja")
        install_dir = os.path.join(PREFIX_PATH, "packages/ninja-v1.8.2")
        if clone_git("git://github.com/ninja-build/ninja.git", build_dir, "v1.8.2"):
            if os.path.exists(install_dir):
                shutil.rmtree(install_dir)
            if subprocess.call(["./configure.py", "--bootstrap"], cwd=build_dir) != 0:
                return

            # Move build binary to package dir
            os.makedirs(install_dir, exist_ok=True)
            bin_path = os.path.join(build_dir, "ninja")
            print("Copying " + bin_path + " to '" + install_dir + "'")
            shutil.copy(bin_path, install_dir)
            shutil.rmtree(build_dir)

            # link built binary in 'bin' directory
            bin_path = os.path.join(install_dir, "ninja")
            dest_path = os.path.join(PREFIX_PATH, "bin/ninja")

            print("Linking " + bin_path + " to '" + dest_path + "'")
            try:
                os.link(bin_path, dest_path)
            except FileExistsError:
                os.unlink(dest_path)
                os.link(bin_path, dest_path)

    if "tmux" in pkgs:
        # Tmux
        required_packages = [
            "gcc",
            "make",
            "autoconf",
            "automake",
            "libevent-devel",
            "ncurses-devel",
        ]
        installed_needed_packages(required_packages)
        build_dir = os.path.join(PREFIX_PATH, "tmux")
        install_dir = os.path.join(PREFIX_PATH, "packages/tmux-v2.7")
        print(build_dir)
        if clone_git("https://github.com/tmux/tmux.git", build_dir, "2.7"):
            build_gmake_config(build_dir, install_dir, autogen=True)

    if "nodejs" in pkgs:
        # NodeJS
        required_packages = ["gcc", "make", "autoconf", "automake"]
        build_dir = os.path.join(PREFIX_PATH, "node")
        install_dir = os.path.join(PREFIX_PATH, "packages/node-v8.11.4")
        installed_needed_packages(required_packages)
        if clone_git("https://github.com/nodejs/node", build_dir, "v10.15.1"):
            build_gmake_config(build_dir, install_dir)


def print_packages():
    """Displays a helpful message to the console showing usage hints."""
    print("TODO: Print helpful messages")


def main():
    """Main loop for build scripts"""
    pkgs: List[str] = None

    parser: argparse.ArgumentParser = argparse.ArgumentParser()
    parser.add_argument(
        "-l",
        "--list-packages",
        action="store_true",
        help="Display list of packages script can compile",
    )
    (options, pkgs) = parser.parse_known_args()

    if options.list_packages:
        print_packages()
        return

    if not pkgs:
        pkgs = ["cmake", "ninja", "tmux", "git", "nodejs"]

    # TODO: Verify package exists
    build(pkgs)


if __name__ == "__main__":
    main()
