#!/usr/bin/env python3
"""
    A script to automate linking of various repo-resident dot files.
    (C)opyright 2017, 2018, 2019 Ron Kuslak, all rights reserved.

    Copyright 2018, 2019 Ron Kuslak

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
"""

import os
import pathlib
import sys
from typing import List, Union

# type for a path that can be expressed as either a string or pathlib.Path
StrOrPath = Union[str, pathlib.Path]

# Parent directory of the consuming script should contain configuration files we want
# to link or copy:
SCRIPT_PATH = pathlib.Path(sys.argv[0]).resolve()
CONFIGS_PATH = SCRIPT_PATH.parent.parent


def link_dotfiles(dotfiles: List[StrOrPath], destination: StrOrPath):
    """
    Create symbolic links for dotfiles.

    Note: for Windows creating symlinks requires a elevated credential, you
    MUST run this from a Administator-joined user or from a user with
    Developer Mode enabled on later versions on Windows 10. As we can not
    ENSURE we can link, files are simply copied instead.
    """

    # Ensure destination directory exists
    if not isinstance(destination, pathlib.Path):
        destination = pathlib.Path(destination)

    os.makedirs(destination, exist_ok=True)

    for dotfile in dotfiles:
        if not isinstance(dotfile, pathlib.Path):
            dotfile = pathlib.Path(dotfile)

        if not dotfile.exists():
            print(f"{dotfile} does not exist, skipping...")
            continue

        filename = dotfile.name
        destpath = destination / filename
        dotfile = CONFIGS_PATH / dotfile

        link_file(dotfile, destpath)


def link_file(dotfile: StrOrPath, destpath: StrOrPath):
    print(f"Linking {dotfile} to {destpath}...")

    if os.name != "nt":
        # Running linux; we can link the files:
        try:
            os.symlink(dotfile, destpath)
            print(f"Linked file {dotfile} to {destpath}")
        except FileExistsError:
            os.unlink(destpath)
            os.symlink(dotfile, destpath)
            print(f"Linked file {dotfile} to {destpath}")

    else:
        # Windows doesn't support linking as non-admin
        import ctypes

        shell32_dll = ctypes.windll.shell32

        # TODO: We should probably ensure we don't need further escaping on this
        #
        # Of note, CMD will not support a shlex-joined string. The escapes for
        # the file names provided will use single-quotes, which the link command
        # will reject. To make this work, we will simply join it manually. This
        # COULD break on a path containing a double-quote, so we likely want to
        # find a more clever way to deal wih this eventually.
        joined_arguments = f'CMD /C MKLINK "{destpath}" "{dotfile}"'

        shell32_dll.ShellExecuteW(None, "runas", "CMD", joined_arguments, None, 1)
