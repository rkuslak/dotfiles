#!/usr/bin/bash

# Script to install from-Docker docker. 

sudo systemctl stop docker
sudo dnf remove docker docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-selinux \
                docker-engine-selinux \
                docker-engine \
                docker-ce \
                docker-ce-cli \
                containerd.io \
                docker-compose

# curl https://download.docker.com/linux/fedora/docker-ce.repo > ./docker-ce.repo
# sudo cp ./docker-ce.repo /etc/yum.repos.d/
# rm ./docker-ce.repo
# sudo dnf install -y docker-ce docker-ce-cli containerd.io docker-compose
sudo dnf install -y moby-engine docker-compose

# Set up networking to accept DNS Masq for Docker, and to disable CGroups v2
sudo firewall-cmd --permanent --zone=trusted --add-interface=docker0
sudo firewall-cmd --permanent --zone=FedoraWorkstation --add-masquerade
sudo grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"

sudo systemctl enable docker
