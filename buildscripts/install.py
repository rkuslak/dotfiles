#!/usr/bin/env python3
""" install.py
    A simple script to install a comfy Fedora desktop

    Copyright 2019 Ron Kuslak

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
"""

import platform
import shutil
import subprocess
import sys
from typing import List

FEDORA_VERSION = subprocess.check_output(["rpm", "-E", "%fedora"]).decode().strip()
DNF_CMD = [
    "sudo",
    "dnf",
    "-y",
]
DNF_INSTALL = DNF_CMD + [
    "install",
]

RPMS = [
    "gnome-tweak-tool",
    "gnome-shell-extension-dash-to-dock",
    "gnome-shell-extension-system-monitor-applet",
    "gnome-shell-extension-openweather",
    "zsh",
]
DEV_RPMS = [
    # Neovim and other CLI tools:
    "ripgrep",
    "bat",
    "tmux",
    "neovim",
    "xclip",
]

DOCKER_RPMS = [
    "moby-engine",
    "docker-compose",
]


def install_compiled_packages():
    # Install LazyGit
    go_programs = {
        "lazygit": "github.com/jesseduffield/lazygit",
    }
    lazyget_install_cmd = [
        "go",
        "get",
    ]

    if shutil.which("go"):
        for cmd, repo in go_programs.items():
            if shutil.which(cmd):
                print(f'"{cmd}" already installed; skipping.')
                continue
            print(f'Installing "{repo}"')
            subprocess.check_call(lazyget_install_cmd + [repo])
    else:
        print("Go does not appear to be installed; skipping...")


def install_rpmfusion():
    """ Installs RPM Fusion repo along with various useful utilities """

    rpms = [
        "gstreamer1-plugins-ugly",
        "vlc",
        "gstreamer1-vaapi",
        "gstreamer1-libav",
        "fuse-exfat",
    ]
    if platform.machine() == "x86_64" or platform.machine() == "i386":
        repo_name = (
            "https://download1.rpmfusion.org/free/fedora/"
            + "rpmfusion-free-release-"
            + FEDORA_VERSION
            + ".noarch.rpm"
        )
        subprocess.check_call(DNF_INSTALL + [repo_name])
        subprocess.check_call(DNF_INSTALL + rpms)


def install_rpms(rpms: List[str]):
    """ Installs the base RPMs needed by the system """
    subprocess.check_call(DNF_CMD + ["upgrade"])
    subprocess.check_call(DNF_INSTALL + rpms)


if __name__ == "__main__":
    passed_args = [arg.lower() for arg in sys.argv]
    rpms = list(RPMS)

    if "--dev" in passed_args:
        rpms.extend(DEV_RPMS)
    if "--docker" in passed_args:
        rpms.extend(DOCKER_RPMS)

    install_rpms(rpms)
    install_compiled_packages()

    install_rpmfusion()
