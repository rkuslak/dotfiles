#!/usr/bin/env python3
"""
    Installs VSCode extentions that are not currently installed.

    Copyright 2018, 2019 Ron Kuslak

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
"""

import logging
import os
import pathlib
import platform
import shutil
import subprocess

import buildtools

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


def get_extension_array():
    """
    Returns a array of extension we expect to need and have installed based on
    the tools available in the current environment and the current OS type.
    """

    extensions = list()

    # Themes:
    extensions += [
        # Themes
        # "gerane.theme-monokai",
        # "AdamCaviness.theme-monokai-dark-soda",
        "gerane.Theme-FlatlandMonokai",
        # "smukkekim.theme-setimonokai",
        "ms-vscode-remote.remote-containers",
    ]

    # General coding utility
    extensions += [
        "2gua.rainbow-brackets",
        "oderwat.indent-rainbow",
        "ronkuslak.dreadalign",
        "wayou.vscode-todo-highlight",
        "stkb.rewrap",
    ]

    # Text editing format extensions:
    extensions += [
        # Enhanced YAML support
        "redhat.vscode-yaml",
    ]

    ## Specific langue support only for basic shell scripting; otherwise, pull into a dev container?
    """
    # Python extensions:
    extensions += [
        "ms-python.python",
        "ms-pyright.pyright",
    ]


    if shutil.which("node"):
        extensions += [
            # HTML / JS / TypeScript / Vue.js / Angular / React:
            # Linters
            "ms-vscode.vscode-typescript-tslint-plugin",
            "dbaeumer.vscode-eslint",
            # Debuggers
            "msjsdiag.debugger-for-chrome",
            "hbenl.vscode-firefox-debug",
        ]

    # Vue support
    if shutil.which("vue"):
        extensions += ["octref.vetur"]

    # Golang support
    if shutil.which("go"):
        extensions += ["golang.Go"]

    if shutil.which("pwsh") or shutil.which("powershell"):
        # Scripting languages:
        extensions += ["ms-vscode.PowerShell"]

    if shutil.which("rustc"):
        # Rust
        extensions += [
            "rust-lang.rust",
            "bungcip.better-toml",
            "serayuzgur.crates",
            "vadimcn.vscode-lldb",
        ]

    if shutil.which("dotnet"):
        # C#:
        extensions += [
            "ms-vscode.csharp",
            "fernandoescolar.vscode-solution-explorer",
            "k--kato.docomment",
        ]

    if os.name == "nt":
        # TSQL:
        extensions += ["TaoKlerks.poor-mans-t-sql-formatter-vscode"]
    """

    LOGGER.debug("Extensions expected to be needed: %s", extensions)
    return extensions


def find_code():
    """
    Finds each installation of VSCode in the path (release or insiders) and
    returns it as a List.
    """

    cmd_names = ["code", "code-insiders"]

    LOGGER.debug("Code installation locations found:")
    for cmd_name in cmd_names:
        LOGGER.debug("\t%s", cmd_name)

    return [shutil.which(cmd) for cmd in cmd_names if shutil.which(cmd)]


def get_installed_exts(code_cmd=""):
    """Returns a list of currently installed extensions for the passed Code
    executable
    """

    if code_cmd:
        exts = subprocess.run(
            [code_cmd, "--list-extensions"], text=True, capture_output=True
        ).stdout.splitlines()
        return [x.lower() for x in exts]

    return []


def install_extentions():
    """ Installs any missing extensions for all found copies of Code """

    cmds = find_code()
    extensions = get_extension_array()

    for cmd in cmds:
        install_extentions = get_installed_exts(cmd)
        for ext in extensions:
            if ext.lower() not in install_extentions:
                LOGGER.warning('Installing extension "%s"', ext)
                subprocess.call([cmd, "--install-extension", ext])


def install_config():
    if not os.name == "nt":
        vscode_path = pathlib.Path(os.environ["HOME"])
        if platform.system() == "Darwin":
            vscode_path = vscode_path / "Library" / "Application Support"
        else:
            vscode_path = vscode_path / ".config"
        vscode_path = vscode_path / "Code" / "User"

    else:
        vscode_path = pathlib.Path(os.getenv("APPDATA")) / "Code" / "User"

    pathlib.Path(vscode_path).mkdir(parents=True, exist_ok=True)
    buildtools.link_dotfiles([buildtools.CONFIGS_PATH / "settings.json"], vscode_path)


if __name__ == "__main__":
    install_config()
    install_extentions()
