#!/usr/bin/env python3

import os
import pathlib
import shutil
import subprocess
import urllib
import urllib.request

import buildtools
from buildtools import link_dotfiles


def install_pip_modules(upgrade=False):
    """
    Install python modules to current user
    """

    pip_modules = ("neovim",)

    if shutil.which("pip3"):
        pip_args = ["pip3"]
    elif shutil.which("pip"):
        pip_args = ["pip"]
    else:
        print("WARNING: Can not find PIP. Can not install Python modules.")
        return

    pip_args += ["install", "--user"]
    if upgrade:
        pip_args += ["--upgrade"]

    for pip_mod in pip_modules:
        subprocess.call(pip_args + [pip_mod])


def install_vim_plug(neovim_root):
    """
    If needed, downloads and installs tpope's Vim Plug-in for vim/neovim.
    """

    autoload_dir = pathlib.Path(neovim_root) / "autoload"
    plug_path = autoload_dir / "plug.vim"
    if plug_path.exists():
        print("Vim Plug appears to be installed; skipping...")
        return

    os.makedirs(neovim_root, exist_ok=True)
    os.makedirs(autoload_dir, exist_ok=True)

    urllib.request.urlretrieve(
        "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim", plug_path
    )


def link_neovim_config(neovim_root):
    neovim_files = [
        buildtools.CONFIGS_PATH / ".config" / "nvim" / "init.lua",
    ]
    buildtools.link_dotfiles(neovim_files, neovim_root)


def find_neovim_root():
    if "HOME" in os.environ:
        neovim_root = pathlib.Path(os.environ["HOME"]) / ".config" / "nvim"
    elif "USERPROFILE" in os.environ:
        neovim_root = pathlib.Path(os.environ["USERPROFILE"])
        neovim_root = neovim_root / "AppData" / "Local" / "nvim"
    else:
        print("Can't find NeoVIM config path; cowardly refusing to install...")

    return neovim_root


def main():
    if not shutil.which("nvim"):
        print("Can't find NeoVIM; cowardly refusing to install...")
        return

    neovim_root = find_neovim_root()
    if not neovim_root:
        return

    install_vim_plug(neovim_root)
    install_pip_modules()
    link_neovim_config(neovim_root)

    # Install VimPlug plugins
    subprocess.call(["nvim", "+:PlugInstall|:PlugClean", "-c", ":qa"])


if __name__ == "__main__":
    main()
