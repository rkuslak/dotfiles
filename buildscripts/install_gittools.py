#!/usr/bin/env python3
import contextlib
import os
import subprocess
import shutil
import time

DOCKER_CMD = "podman" if shutil.which("podman") else "docker"


def create_image():
    build_cmd = [
        DOCKER_CMD,
        "build",
        "-t",
        "gittools",
        "-f",
        "build_git_tools.dockerfile",
        ".",
    ]
    subprocess.check_call(build_cmd)


@contextlib.contextmanager
def container_context():
    timestamp_int = int(time.time())
    container_name = f"bintools_{timestamp_int}"

    teardown_cmd = [
        DOCKER_CMD,
        "container",
        "stop",
        container_name,
    ]
    build_cmd = [
        DOCKER_CMD,
        "run",
        "--rm",
        "-d",
        "--name",
        container_name,
        "-it",
        "gittools",
        "bash",
    ]

    subprocess.check_call(build_cmd)
    try:
        yield container_name
    finally:
        subprocess.check_call(teardown_cmd)


def install_bins():
    bins = [
        "lazygit",
        "gitui",
    ]

    local_path = os.getenv("HOME") + "/.local/bin/"

    copy_command_base = [DOCKER_CMD, "cp"]

    with container_context() as container_name:
        for bin_name in bins:
            copy_command = list(copy_command_base)
            source_path_str = f"{container_name}:/bintray/{bin_name}"
            dest_path_str = f"{local_path}{bin_name}"

            copy_command.extend([source_path_str, dest_path_str])

            subprocess.check_call(copy_command)


if __name__ == "__main__":
    create_image()
    install_bins()
