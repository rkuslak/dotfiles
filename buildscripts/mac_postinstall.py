#!/usr/bin/env python
"""
Simple script to set up basic Homebrew packages needed, and download/install
Homebrew if not already installed. Assume we can only use Python 2.7, as we're
likely installing homebrew FOR Python 3.x

    Copyright 2019 - 2020 Ron Kuslak

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
"""

from __future__ import print_function

import distutils.spawn
import os
import os.path
import subprocess
import sys

import urllib2

CASKS = [
    "docker",
    "iterm2",
    "scroll-reverser",
    "visual-studio-code",
    "font-fira-mono",
    "keepassxc",
    "sensiblesidebuttons",
    "vlc",
    "krita",
]

PACKAGES = [
    "neovim",
    # Languages / development utilities
    "cmake",
    "python",
    # "go",
    "rust",
    # Various git utilities
    "gitui",
    "lazygit",
    "tig",
    # Terminal utilities
    "bat",
    "sd",
    "tmux",
]


def enable_click_to_move_and_resize():
    cmd = [
        "defaults",
        "write",
        "-g",
        "NSWindowShouldDragOnGesture",
        "-bool",
        "true",
    ]
    subprocess.call(cmd)


def install_brew(install_path):
    bin_path = os.path.join(install_path, "bin")

    if not os.path.exists(install_path):
        os.makedirs(install_path)

    if not distutils.spawn.find_executable("brew"):
        print("Homebrew not installed. Pulling down and unpacking to %s" % install_path)

        BREW_URL = "https://github.com/Homebrew/brew/tarball/master"
        TAR_PATH = os.path.join(os.environ["HOME"], "homebrew.tar.gz")
        request_reader = urllib2.urlopen(BREW_URL)

        with open(TAR_PATH, "w") as f:
            f.write(request_reader.read())
            f.close()

        subprocess.call(["tar", "xzf", TAR_PATH, "--strip", "1", "-C", install_path])

    # Ensure we've added the bin path to .profile if not already added
    abs_paths = [os.path.abspath(x) for x in os.environ["PATH"].split(":")]
    if bin_path not in abs_paths:
        profile_path = os.path.join(os.environ["HOME"], ".profile")

        with open(profile_path, "a+") as profile_file:
            profile_file.write("\nexport PATH=${PATH}:~/homebrew/bin")

        print(
            "Added homebrew to path; you will need to restart shell for it to take effect"
        )


def install_packages(install_path):
    bin_path = os.path.join(install_path, "bin")
    brew_binary_path = os.path.join(bin_path, "brew")

    subprocess.call([brew_binary_path, "install"] + PACKAGES)

    if "--no-cask" in [x.lower() for x in sys.argv]:
        return

    subprocess.call([brew_binary_path, "install"] + CASKS)


if __name__ == "__main__":
    install_path = os.path.join(os.environ["HOME"], "homebrew")

    enable_click_to_move_and_resize()

    install_brew(install_path)
    install_packages(install_path)
