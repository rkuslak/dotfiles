FROM rust:latest as rust_build

RUN mkdir /bintray && \
	apt-get update && \
	apt-get -y install libxcb-xfixes0-dev libxcb-shape0-dev
RUN cargo install gitui && cp $(which gitui) /bintray

FROM golang:latest as golang_build

RUN mkdir /bintray

COPY --from=rust_build /bintray/* /bintray

RUN go get github.com/jesseduffield/lazygit && cp $(which lazygit) /bintray/
