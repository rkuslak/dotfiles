#!/usr/bin/env python3
import os
import pathlib
import shutil
import subprocess


def install_apt_packages():
    package_list = [
        "neovim",
        "python3-venv",
        "python3-pip",
        "make",
        "gcc",
        "ninja-build",
        "cmake",
        "tig",
        "zsh",
    ]
    # TODO: Add in desktop apps if we're not under WSL? Will we ever run Ubuntu
    # outside WSL?

    subprocess.check_call(["sudo", "apt", "update"])
    subprocess.check_call(["sudo", "apt", "upgrade", "-y"])

    install_cmd = ["sudo", "apt", "install", "-y"]
    subprocess.check_call(install_cmd + package_list)


def install_git_tools():
    LOCAL_BUILDS_PATH = pathlib.Path(os.environ["HOME"]) / "local"
    build_image_cmd = [
        "docker",
        "build",
        "-t",
        "git_tools",
        "-f",
        "./build_git_tools.dockerfile",
        ".",
    ]
    install_tools_cmd = [
        "docker",
        "run",
        "--rm",
        "-v",
        f"{LOCAL_BUILDS_PATH}:/opt:Z",
        "-t",
        "git_tools",
        "sh",
        "-c",
        "cp /bintray/* /opt/bin",
    ]
    subprocess.check_call(build_image_cmd)
    subprocess.check_call(install_tools_cmd)


def install_fonts():
    fira_link = "https://fonts.google.com/download?family=Fira%20Mono"


if __name__ == "__main__":
    install_apt_packages()
    install_git_tools()
