# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

source ~/.profile

# Some QOL settings liberally borrowed from
# https://github.com/mattmc3/bash-sensible/
bind "set completion-ignore-case on"
bind "set completion-map-case on"

export JAVA_HOME="/usr/lib/jvm/java-1.8.0/"

if [ -f /etc/redhat-release ] && cat /etc/redhat-release | grep 'CentOS' &> /dev/null; then
    # Set centos-exclusive values here:
    alias nvim="TERM=screen-256color nvim"
fi

PS1="\[\e[0;37m\][\[\e[32;1m\]\u\[\e[0;32m\]@\[\e[30;1m\]\h\[\e[0;37m]\[\e[0m\]\n"
export PS1="${PS1}\[\e[36;1m\]\w \[\e[0m\]$ \[\e[0m\]"
