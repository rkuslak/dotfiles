source ~/.profile

# Provide the current git branch on a line by itself if in one; if not, provide
# nothing. Thus we can assume that for the prompt, we will have it on a entirely
# new line.
autoload -Uz vcs_info
setopt PROMPT_SUBST
zstyle ':vcs_info:git*' formats "%F{grey}[%F{green}%b%F{grey}]${NEWLINE}"
precmd() { vcs_info }

NEWLINE=$'\n'
export PS1='%F{grey}[%B%F{green}%n%b@%F{black}%B%M%b%F{grey}]${NEWLINE}%F{white}%B%*${NEWLINE}${vcs_info_msg_0_}%F{cyan}%B%~%b%f '

# Since editor is VIM it defaults to VIM keybinds; explicitly use Emacs
# keybinds:
set -o emacs

# some pleasant defaults to clean up after ourselves in the history:
setopt HIST_REDUCE_BLANKS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_SAVE_NO_DUPS

bindkey "[3~" delete-char
bindkey "[H" beginning-of-line
bindkey "[F" end-of-line

bindkey "[1;3C" forward-word
bindkey "[1;3D" backward-word

bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word

# Enable history
HISTFILE="${HOME}/.zsh_history"
SAVEHIST=2000

# If we have pyenv, enable it:
if [ -f "`which pyenv 2> /dev/null`" ]; then
    eval "$(pyenv init -)"
fi

if [ -f "`which python3 2> /dev/null`" -a -f "`which brew 2> /dev/null`" ]; then
    alias python3="${HOME}/homebrew/bin/python3"
    alias pip3="${HOME}/homebrew/bin/pip3"
fi

ZSH_COMPLETIONS="${HOME}/homebrew/share/zsh/site-functions/"
if [ -d ${ZSH_COMPLETIONS} ]; then
    export FPATH=${ZSH_COMPLETIONS}:${FPATH}
    rm -rf ${HOME}/.zcompdump
    autoload -Uz compinit && compinit
fi

# Enable case insensitive path completions
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*'
